import { render } from 'preact'

import { App } from './app'

export const main = () => {
	const wrap = document.createElement('main')
	document.body.appendChild(wrap)
	render(<App />, wrap)
}
