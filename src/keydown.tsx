import { useEffect, useRef } from 'preact/hooks'

export const Keydown = () => {
	const input = useRef<HTMLInputElement>(null)
	useEffect(() => {
		const handler = (e: unknown) => {
			const ipt = input.current
			if (!ipt) return
			console.log({ e })
			ipt.focus()
		}
		document.body.addEventListener('keydown', handler)
		return () => {
			document.body.removeEventListener('keydown', handler)
		}
	}, [])
	return (
		<div>
			<input type="text" ref={input} />
		</div>
	)
}
